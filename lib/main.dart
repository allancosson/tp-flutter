import 'dart:math';

import 'package:json_theme/json_theme.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:initiation_flutter/my_flutter_app_icons.dart';
import 'package:initiation_flutter/screens/acceuil.dart';
import 'package:initiation_flutter/screens/pageParametre.dart';
import 'package:initiation_flutter/screens/pageSuivante.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final themeStr = await rootBundle.loadString('assets/appainter_theme.json');
  final themeJson = jsonDecode(themeStr);
  final theme = ThemeDecoder.decodeThemeData(themeJson);

  runApp(MyApp(theme : theme));
}

class MyApp extends StatelessWidget {
final ThemeData? theme;

  const MyApp({Key? key, required this.theme}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(  
      title: 'Découverte de Flutter',
      debugShowCheckedModeBanner: false,
      theme: theme,
      home: const MyHomePage(title: "L'application incroyable de Allan"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;
  

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int currentPageIndex = 0;
  
  _getDrawerItemWidget(int pos){
    switch (pos){
      case 0:
        return new AcceuilBody();
      case 1:
        return new SuivantBody();
      case 2:
        return new ParamBody();
      default:
        return new Text("Erreur de page");      
    }
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Column(
      children: [
        AppBar(
          key: Key('appBarPrincipale'),
          title: Text('Aléatoire et Dés'),
        ),
        
        Expanded(
          child: Scaffold(
            body: _getDrawerItemWidget(currentPageIndex),
          ),
        ),
        NavigationBar(
          onDestinationSelected: (int index) {
            setState(() {
            currentPageIndex = index;
          });
        },
	      selectedIndex: currentPageIndex,

        destinations: const <Widget>[
          NavigationDestination(
            icon: Icon(Icons.home),
            label: 'Accueil',
          ),
          NavigationDestination(
            icon: Icon(Icons.skip_next),
            label: 'Page suivante',
          ),
          NavigationDestination(
            icon: Icon(MyFlutterApp.cog),
            label: 'Paramètres',
          ),
        ],
      ),
      ],
    );
  }




  /*
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
          currentPageIndex = index;
        });
      },
      selectedIndex: currentPageIndex,

      destinations: const <Widget>[
        NavigationDestination(
          icon: Icon(Icons.home),
          label: 'Accueil',
        ),
        NavigationDestination(
          icon: Icon(Icons.skip_next),
          label: 'Page suivante',
        ),
        NavigationDestination(
          icon: Icon(Icons.app_settings_alt),
          label: 'Paramètres',
        ),
      ],
      ),
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),      
      ),
      body:  <Widget>[
              Scaffold(       
                body: Center(
                  child:ListView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(20.0),
                    children : [
                      Image.asset('images/shrek.jpg',
                        width: 150,
                        height: 150,
                      ),
                      const Text(
                        'Nombre :',
                      ), 
                      Text(
                        '$_counter',
                        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                      ),
                      Column(
                children: const [
                  SizedBox(
                    height: 50,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      'Lorem Ipsum',
                      style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              )
                    ],
                  ),
                )  
              ),
              Container(
                color: Colors.green,
                alignment: Alignment.center,
                child:ListView (
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(20.0),
                  children: <Widget>[
                    const Text('Page suivante'),
                    Text(
                        '$_counter',
                        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                      ),
                    Padding(padding: EdgeInsets.only(left:31),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: FloatingActionButton(
                        onPressed: _nbaleatoire,
                        child: Text("Nb aléatoire"),),
                    ),),    
                    
                     
                  ]
                  ),
              ),
              Container(
                color: Colors.red,
                alignment: Alignment.center,
                child: const Text('Page suivante'),
              ),
            ][currentPageIndex],

    floatingActionButton: Stack(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(left:31),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton(
                onPressed: _decrementCounter,
                child: Icon(Icons.exposure_minus_1),),
            ),),
            Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                onPressed: _incrementCounter,
              child: Icon(Icons.plus_one),),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: FloatingActionButton(
                onPressed: _reset,
              child: Text('0')),
            ),
          ],
        )


      );
    }
  }
  */
}