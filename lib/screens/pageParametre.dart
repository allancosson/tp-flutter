import 'package:flutter/material.dart';
import 'album.dart';

class ParamBody extends StatefulWidget {
  const ParamBody({Key? key}) : super(key: key);

  @override
  State<ParamBody> createState() => _ParamBody();
}


class _ParamBody extends State<ParamBody> {


  @override
  Widget build(BuildContext context) {
    return Container(
    color: Colors.blue,
    alignment: Alignment.center,
    child: ListView(
      shrinkWrap: true, padding: const EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
      children: <Widget>[
        Album(
          nomAlbum: "Metallica",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Metallica.jpg",
          favoriAlbum: false,
        ),
                Album(
          nomAlbum: "Ride the lightning",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Ride the lightning.jpg",
          favoriAlbum: false,
        ),
                Album(
          nomAlbum: "Matser of Puppets",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Matser of puppets.jpg",
          favoriAlbum: false,
        ),
                Album(
          nomAlbum: "And Justice for All",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "And justice for All.jpg",
          favoriAlbum: false,
        ),
      ],
    ),
    ); 
  }
}
