import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initiation_flutter/screens/splashscreen.dart';
import './unAlbum.dart';

class Album extends StatelessWidget {
  Album({Key? key, this.nomAlbum, this.description, this.nomGroupe, this.image, this.favoriAlbum})
    : super(key: key);
  final String? nomAlbum;
  final String? description;
  final String? nomGroupe;
  final String? image;
  final bool? favoriAlbum;
  
  
  
  Widget build(BuildContext context){
    return Container(
      padding: EdgeInsets.all(2), height: 200, child:GestureDetector(
              onTap: () {
              SystemChrome.setEnabledSystemUIMode(
               SystemUiMode.immersive,
              );
              Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ecranDemarragePage(nomAlbum: nomAlbum, description: description, nomGroupe: nomGroupe, image: image,),
              ),
            );
          },
        child: Card(        
          child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
            Image.asset("images/"+image!), Expanded(
              child: Container(
                padding: EdgeInsets.all(5), child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                    Text(this.nomAlbum!, style: TextStyle(fontWeight: FontWeight.bold)),
                    Text(this.description!),
                    Text("Valeur : "+ this.nomGroupe.toString()),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    ),
    );
  }
}

/*
class _unAlbumState extends State<unAlbum>{
  late String? album;

  @override
  void initState(){
    super.initState();
    album = widget.nomAlbum!;
  }

  Widget build(BuildContext context){
    return Container();
  }
}
*/