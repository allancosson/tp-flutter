import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splashscreen/splashscreen.dart';
import './unAlbum.dart';

class ecranDemarragePage extends StatelessWidget{

  const ecranDemarragePage({Key? key, this.nomAlbum, this.description, this.nomGroupe, this.image})
    : super(key: key);
    final String? nomAlbum;
    final String? description;
    final String? nomGroupe;
    final String? image;

  @override
  Widget build(BuildContext context){
    return SplashScreen.timer(
      seconds: 1,
      navigateAfterSeconds: unAlbum(nomAlbum: nomAlbum, description: description, nomGroupe: nomGroupe, image: image,),
      backgroundColor: Colors.green,
      title: Text('Chargement', textScaleFactor: 2,),
      image: Image.network(
        'https://d1fmx1rbmqrxrr.cloudfront.net/cnet/optim/i/edit/2016/08/fabrication-vinyl__w770.jpg'
      ),
      loadingText: Text("Loading"),
      photoSize: 110.0,
      loaderColor:Colors.red
    );
  }
}
