import 'dart:math';
import 'package:flutter/material.dart';

class SuivantBody extends StatefulWidget {
  const SuivantBody({Key? key}) : super(key: key);

  @override
  State<SuivantBody> createState() => _SuivantBody();
}


class _SuivantBody extends State<SuivantBody> {
  int _nb1 = 0;
  int _nb2 = 0;
  int _reponse = 0;
  int _counterRandom = 0;
  List<String> listNb = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"];
  int index = 0;
  String _valueDropDown = "";
  
  void _nbaleatoire() {
    setState(() {
      _nb1 = Random().nextInt(10);
      _nb2 = Random().nextInt(10);
      if(_nb1 + _nb2 > index){
        _nbaleatoire();
      }
    });
  }

  void _showToast(BuildContext context, String _message){
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(content: Text(_message),),
    );
  }

  void _verifValeur(){
    setState(() {
      if(_reponse == (_nb1 + _nb2)){
        _showToast(context, "C'est une bonne réponse");
      }
      else{
        _showToast(context, "C'est une mauvaise réponse");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.green,
        alignment: Alignment.center,
        child:ListView (
          shrinkWrap: true,
          padding: const EdgeInsets.all(20.0),
          children: <Widget>[
            const Text('Page suivante'),
            Text(
                '$_nb1 + $_nb2 = ?',
                style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
              ),
            TextField(
              keyboardType: TextInputType.number,
              maxLength: 25,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                ),
                decoration: const InputDecoration(
                  labelText: 'Entrer la solution',
                  hintText: 'Entrer un nombre',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {_reponse = int.parse(value);}
            ),
            Padding(padding: EdgeInsets.only(left:31),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton(
                onPressed: _nbaleatoire,
                child: Text("Nb aléatoire"),),
            ),
            ),
            Padding(padding: EdgeInsets.only(left:50),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton(
                onPressed:(){
                  _verifValeur();
                },
                child: Text("Valider"),),
            ),
            ),  
            DropdownButton(
                icon: AlertDialog(),
                value: index.toString(),
                items: listNb.map<DropdownMenuItem<String>>((String value){
                  return DropdownMenuItem<String>(value: value ,child: Text(value),);
                }).toList(), 
                onChanged: (String? value) {
                  setState((){
                    index = int.parse(value!);
                  });
                },
              ),
          ],
        ),
    ); 
  }
}