import 'package:flutter/material.dart';

class unAlbum extends StatefulWidget{
  const unAlbum({Key? key, this.nomAlbum, this.description, this.nomGroupe, this.image})
    : super(key: key);
    final String? nomAlbum;
    final String? description;
    final String? nomGroupe;
    final String? image;
  
  @override
  State<unAlbum> createState() =>_unAlbumState();
}

/*
class ParamBody extends StatefulWidget {
  const ParamBody({Key? key}) : super(key: key);

  @override
  State<ParamBody> createState() => _ParamBody();
}
*/


class _unAlbumState extends State<unAlbum>{
  late String? album;
  late String? description;
  late String? groupe;
  late String? image;
  bool? favoriAlbum = false;

  void retour(){
    Navigator.pop(context);
  }

  void _toggleFavorite(String rechercheIndex) {
    int index = ListeAlbumDesc.indexWhere((element) => element['nomAlbum'] == rechercheIndex);
    setState(() {
      favoriAlbum = true; // nouvelle valeur du bouton;
      if (index != -1) {
        ListeAlbumDesc[index]['favori'] =// état du bouton ;
      }
    });
  }

  @override
  void initState(){
    super.initState();
    album = widget.nomAlbum!;
    description = widget.description;
    groupe = widget.nomGroupe;
    image = widget.image;
  }

  void toggleFavori(){
    setState(() {
      favoriAlbum = !favoriAlbum!;
    });
  }

  Widget build(BuildContext context){
    return Container(
      color: Color.fromARGB(255, 191, 199, 172),
      alignment: Alignment.center,
      child: ListView(
      children: <Widget>[
        FloatingActionButton(
          onPressed:(){
            retour();
          },
         child: Text("<--"),),
         FloatingActionButton(onPressed: _toggleFavorite(album), child: Icon(favoriAlbum! ? Icons.star : Icons.star_border, color: favoriteAlbum! ? Colors.black : null,),),
          Image.asset("images/"+image!),
          Text("$album", style: TextStyle(color: Colors.black, fontSize: 16),),
          Text("$description", style: TextStyle(color: Colors.black, fontSize: 16),),
        ],
      ),
    );
  }
}