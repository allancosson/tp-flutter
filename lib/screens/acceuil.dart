import 'dart:math';

import 'package:flutter/material.dart';
import 'package:initiation_flutter/main.dart';
import 'album.dart';


/*
void main() {
  runApp(const AcceuilBody());
}
*/

class AcceuilBody extends StatefulWidget{
  const AcceuilBody({Key? key}) : super(key: key);  

  @override
  State<AcceuilBody> createState() => _AcceuilBody();
  Widget build(BuildContext context) {
    return MaterialApp(  
      title: 'Découverte de Flutter',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Color.fromARGB(255, 201, 7, 7)),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: "L'application incroyable de Allan"),
    );
  }
}

class _AcceuilBody extends State<AcceuilBody>{
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }
  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }
    void _reset() {
    setState(() {
      _counter = 0;
    });
  }
  void _nbaleatoire() {
    setState(() {
      _counter = Random().nextInt(100);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
    color: Colors.blue,
    alignment: Alignment.center,
    child: ListView(
      shrinkWrap: true, padding: const EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
      children: <Widget>[
        Album(
          nomAlbum: "Metallica",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Metallica.jpg",
        ),
                Album(
          nomAlbum: "Ride the lightning",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Ride the lightning.jpg",
        ),
                Album(
          nomAlbum: "Matser of Puppets",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "Matser of puppets.jpg",
        ),
                Album(
          nomAlbum: "And Justice for All",
          description: "L'album marque une évolution importante dans le style du groupe. Les tempos sont plus lents, les morceaux plus courts et leurs structures beaucoup plus simples, aspirant ainsi à du simple rock. C'est principalement un album de heavy metal, et il n'y a plus beaucoup de traces de thrash metal. ",
          nomGroupe: "Metallica",
          image: "And justice for All.jpg",
        ),
      ],
    ),
    ); 
  }

}
