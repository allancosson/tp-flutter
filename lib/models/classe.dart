import 'package:flutter/material.dart';

class ParamBody extends StatefulWidget{
  const ParamBody({Key? key}) : super(key: key);

  @override
  State<ParamBody> createState() => _ParamBody();
}

class _ParamBody extends State<ParamBody>{
  int currentPageIndex = 0;
  @override
  Widget build(BuildContext context){
    return Container(
      color: Colors.blue,
      alignment: Alignment.center,
      child: const Text("Paramètres"),
    );
    bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
		      currentPageIndex = index;
        });
        },
	  selectedIndex: currentPageIndex,

        destinations: const <Widget>[
          NavigationDestination(
            icon: Icon(Icons.home),
            label: 'Accueil',
          ),
          NavigationDestination(
            icon: Icon(Icons.skip_next),
            label: 'Page suivante',
          ),
          NavigationDestination(
            icon: Icon(Icons.app_settings_alt),
            label: 'Paramètres',
          ),
        ],
      );
  }
}